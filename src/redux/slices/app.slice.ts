import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export interface AppSliceState {
  booted: boolean;
}

const initialData: AppSliceState = {
  booted: false,
};

export const appSlice = createSlice({
  name: 'app',
  initialState: initialData,
  reducers: {
    bootApp(state, action) {
      state.booted = action.payload;
    },
  },
});

export const {bootApp} = appSlice.actions;
