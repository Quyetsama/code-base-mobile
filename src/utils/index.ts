import {Dimensions, Platform, StatusBar} from 'react-native';

export const {width, height} = Dimensions.get('window');
export const statusBarHeight: number = StatusBar.currentHeight
  ? StatusBar.currentHeight
  : 0;

export const isIos = Platform.OS === 'ios';

export const colors = {
  primary: '#40C492',
  onPrimary: '#FFFFFF',
  secondary: '#FFA25B',
  background: '#FFFFFF',
  background2: '#F9F9F9',
  onBackground: '#292C38',
  title: '#1F244B',
  label: '#616161',
  text: '#A1A2A9',
  danger: '#E44F0B',
  grey1: '#2C2C2E',
  grey2: '#8E8E93',
  grey3: '#dedee3',
  bubble: '#007BFF10',
  textMessage: '#737373',
  inputBackground: '#F7F8FC',
  green: '#329F50',
  border: '#EAEAEA',
  border2: '#BBBBBB',
  underlayColor: '#09264210',
};
