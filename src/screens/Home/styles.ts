import {StyleSheet} from 'react-native';
import {colors} from '@utils/index';

const styles = StyleSheet.create({
  outer: {
    flex: 1,
  },
});

export default styles;
